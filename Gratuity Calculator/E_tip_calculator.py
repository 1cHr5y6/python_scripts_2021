print('')
print('')
print('=================================================')
print('====                                         ====')
print('====       Gratuity Calculator Program       ====')
print('====                                         ====')
print('=================================================')
print('')

# Ask user input for bill amount
user_input = input('Enter the total bill amount: ')

# Remove the commas, if any, from the user input
initial_user_input = user_input
bill = initial_user_input.replace(',' , '  ')

# Remove the spaces from the previous step
total_bill = bill.replace(' ', '')

# Uncomment the two lines below to debug 
#print('DEBUG total_bill:           ', total_bill)
#print('') 

# Ask user input to calculate gratuity
user_input2 = input("Enter a whole number to calculate tip percentage: ")
tip_as_percent = float(user_input2) / float(100)

# Uncomment line below to debug
#print('DEBUG: tip_as_percent            ', tip_as_percent)

# Format tip amount in dollars
FORMAT_total_tip_amount2 = '${:,.2f}'.format(float(total_bill) * float(tip_as_percent))
# Uncomment line below to debug
#print('DEBUG: FORMAT_total_tip_amount2  ', FORMAT_total_tip_amount2)

# Calculate tip amount in dollars
total_tip_amount2 = '{:,.2f}'.format(float(total_bill) * float(tip_as_percent))
#print('DEBUG: total_tip_amount2:         ', total_tip_amount2)

# Remove the comma from the total_tip_amount2
total_tip_amount2_FINAL = total_tip_amount2.replace(',' , '')
#print('DEBUG: total_tip_amount2_FINAL    ', total_tip_amount2_FINAL)

# Calculate the total bill, including gratuity
TEST_bill_with_tip = float(total_bill) + float(total_tip_amount2_FINAL)
# Add dollar sign and commas to format FORMAT_TEST_bill_with_gratuity
FORMAT_TEST_bill_with_tip = '${:,.2f}'.format(TEST_bill_with_tip)
print('')
#print('DEBUG: FORMAT_TEST_bill_with_tip      ', FORMAT_TEST_bill_with_tip)


# Print RAW_TEST_bill_with_tip without the dollar sign
RAW_TEST_bill_with_tip = '{:,.2f}'.format(TEST_bill_with_tip)
#print('DEBUG: RAW_TEST_bill_with_tip:         ', RAW_TEST_bill_with_tip)

# Remove comma from RAW_TEST_bill_with_tip_FINAL
RAW_TEST_bill_with_tip_FINAL = RAW_TEST_bill_with_tip.replace(',', '')
#print('DEBUG: RAW_TEST_bill_with_tip_FINAL:   ', RAW_TEST_bill_with_tip_FINAL)

# Header for Summary 
print('')
print('--------------------------------------------------')
print('-----        TOTAL BILL WITH GRATUITY        -----')
print('--------------------------------------------------')
print('')

# Ask for user input to divide bill based upon number of people  
people = int(input('How many people to divy the bill amongst?: '))
print('')

# Calculate amount each person pays
bill_per_person = float(RAW_TEST_bill_with_tip_FINAL) / int(people)
#print('DEBUG: bill_per_person ', bill_per_person)

# Format the amount each person pays, using dollar sign and comma
final_amount = '${:,.2f}'.format(float(bill_per_person))
print("Each person should pay: {0}".format(final_amount))
print('')

