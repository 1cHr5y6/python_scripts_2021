# menu_selection.py

from termcolor import colored
from pizza_menu import pizza_menu
from clear import clear
from time import sleep


small_pizza =  15
small_C_only = 16
small_P_only = 17
small_B      = 18

medium_pizza  = 20
medium_C_only = 21
medium_P_only = 23
medium_B      = 24

large_pizza  = 25
large_C_only = 26
large_P_only = 28
large_B      = 29

pepperoni_small = 2
pepperoni_medium = 3
pepperoni_large = 3
extra_cheese = 1


def menu_selection():
    pizza_menu()
    pizza_size = input("Enter a size ( S M L ):   ")
    pizza_size_2 = pizza_size
    #print("DEBUG: line 9" + pizza_size_2)

    print("")
    print("Select from the following: ")
    print(" \'C\' for extra cheese")
    print(" \'P\' for pepperoni ")
    print(" \'B\' for both extra cheese and pepperoni  ")    
    print("")
    pizza_toppings = input("Enter the additional toppings for your order:  ")
    if pizza_toppings == "C" or pizza_toppings == "c":
        pizza_toppings_2 = "extra cheese"
        small_pizza_C_total = small_C_only  
        medium_pizza_C_total = medium_C_only
        large_pizza_C_total = medium_C_only
    elif pizza_toppings == "P" or pizza_toppings == "p":
        pizza_toppings_2 = "pepperoni"
        small_pizza_P_total = small_P_only
        medium_pizza_P_total = medium_P_only
        large_pizza_P_total = large_P_only
    else:
        pizza_toppings_2 = "extra cheese and pepperoni"
        small_pizza_B_total = small_B
        medium_pizza_B_total = medium_B
        large_pizza_B_total = large_B
        

    if pizza_size_2 == "S" or pizza_size_2 == "s":
        print("")
        print("")
        print('\x1b[2;30;43m' + '______ O R D E R _____C O N F I R M A T I O N ________' + '\x1b[0m')
        print(f"You have ordered a small pizza with {pizza_toppings_2}.") 
        print("")
        confirm = input("Confirm the order is correct to proceed to the payment screen: (Y/N) ")
        sleep(3)
        clear()


        print('\x1b[2;30;43m' + '______P A Y M E N T  ____   _S C R E E N ________' + '\x1b[0m')
        if confirm == "Y" or confirm == "y":
            if pizza_toppings == "C" or pizza_toppings == "c":
                small_C_total = '${:,.2f}'.format(small_pizza_C_total)
                print("")
                print(colored("Your total is: ", "yellow") + small_C_total)
            elif pizza_toppings == "P" or pizza_toppings == "p":
                small_P_total = '${:,.2f}'.format(small_pizza_P_total)
                print("")
                print(colored("Your total is: ", "yellow") + small_P_total)
            else:
                small_B_total = '${:,.2f}'.format(small_pizza_B_total)
                print("")
                print(colored("Your total is: ", "yellow") + small_B_total)
               
            print("")
            print(colored("When prompted for payment: ", "yellow"))
            print("Enter (C) for Cash ")
            print("      (S) for Student Meal Pass ")
            print("")
            print("")
            payment_type = input(colored("Would you like to pay by cash or Student Meal Pass?  \n ", "yellow"))
            if payment_type == "C" or payment_type == "c":
                print("")
                print(colored("Example: To pay $30 enter 30.00 or 30 ", "yellow"))
                cash = input(colored("Enter the amount you would like to pay in cash: \n ", "yellow"))
                cash_E = cash.replace('.', ' ')
                cash_E1 = cash_E.replace(' ', '')
                if int(cash_E1) < int(small_B):
                    print("Invalid entry.")
                    print("")
                    cash2 = input(colored("Enter a valid amount to pay for the order. \n", "yellow"))
                    cash2_E = cash2.replace('.', ' ')
                    cash2_E1 = cash2_E.replace(' ', '')
                    if int(cash2_E1) < int(small_B):
                        print("Invalid entry. Order canceled.")  
                    else:
                        if int(cash2_E1) > int(small_B):
                            cash_back = int(cash2_E1) - int(small_B) 
                            print("")
                            print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
                            print("")
                            print("")
                else:
                    print("")
                    print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
            elif payment_type == "S" or payment_type == "s":
                print("")
                student_id = input("Enter your Student ID: ")
                print("")
                print("Thank you! Your order will arrive shortly.") 
                print("The total will be deducted from your student account.")
                print("")
                print("")
                print("")
            else:
                print("Invalid entry.")

    
    if pizza_size_2 == "M" or pizza_size_2 == "m":
        print("")
        print("")


        print('\x1b[2;30;43m' + '______ O R D E R _____C O N F I R M A T I O N ________' + '\x1b[0m')
        print(f"You have ordered a medium pizza with {pizza_toppings_2}.") 
        print("")
        confirm = input("Confirm the order is correct to proceed to the payment screen: (Y/N) ")
        sleep(3)
        clear()


        print('\x1b[2;30;43m' + '______   P A Y M E N T  _____   S C R E E N ________' + '\x1b[0m')
        if confirm == "Y" or confirm == "y":
            if pizza_toppings == "C" or pizza_toppings == "c":
                medium_C_total = '${:,.2f}'.format(medium_pizza_C_total)
                print("")
                print(colored("Your total is: ", "yellow") + medium_C_total)
            elif pizza_toppings == "P" or pizza_toppings == "p":
                medium_P_total = '${:,.2f}'.format(medium_pizza_P_total)
            else:
                medium_B_total = '${:,.2f}'.format(medium_pizza_total)
                print("")
                print(colored("Your total is: ", "yellow") + medium_B_total) 


            print("") 
            print(colored("When prompted for payment: ", "yellow"))
            print("Enter (C) for Cash  ")
            print("      (S) for Student Meal Pass  ")
            print("")
            print("")
            payment_type = input(colored("Would you like to pay by cash or Student Meal Pass? \n  ", "yellow"))
            if payment_type == "C" or payment_type == "c":
                print("")
                print(colored("Example: To pay $30 enter 30.00 or 30 ", "yellow"))
                cash = input(colored("Enter the amount you would like to pay in cash: \n  ", "yellow"))
                cash_E = cash.replace('.', ' ')
                cash_E1 = cash_E.replace(' ', '')
                if int(cash_E1) < int(medium_B):
                    print("Invalid entry.")
                    print("")
                    cash2 = input(colored("Enter a valid amount to pay for the order.  \n", "yellow"))
                    cash2_E = cash2.replace('.', ' ')
                    cash2_E1 = cash2_E.replace(' ', '')
                    if int(cash2_E1) < int(medium_B):
                        print("Invalid entry. Order canceled.")  
                    else:
                        if int(cash2_E1) > int(medium_B):
                            cash_back = int(cash2_E1) - int(medium_B) 
                            print("")    
                            print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
                            print("")
                            print("")
                else:
                    print("")
                    print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
            elif payment_type == "S" or payment_type == "s":
                print("")
                student_id = input("Enter your Student ID: ")
                print("")
                print("Thank you! Your order will arrive shortly.")
                print("The total will be deducted from your student account.")
                print("")
                print("")
                print("")
            else:
                print("Invalid entry.")

    if pizza_size_2 == "L" or pizza_size_2 == "l":
        print("")
        print("")


        print('\x1b[2;30;43m' + '______ O R D E R _____C O N F I R M A T I O N ________' + '\x1b[0m')
        print(f"You have ordered a large pizza with {pizza_toppings_2}.") 
        print("")
        confirm = input("Confirm the order is correct to proceed to the payment screen: (Y/N) ")
        sleep(3)
        clear()


        print('\x1b[2;30;43m' + '______P A Y M E N T  _____S C R E E N ________' + '\x1b[0m')
        if confirm == "Y" or confirm == "y":
            if pizza_toppings == "C" or pizza_toppings == "c":
                large_C_total = '${:,.2f}'.format(large_pizza_C_total)
                print("")
                print(colored("Your total is:  ", "yellow") + large_C_total)
            elif pizza_toppings == "P" or pizza_toppings == "p":
                large_P_total = '${:,.2f}'.format(large_pizza_P_total)
                print(colored("Your total is:  ", "yellow") + large_P_total)
            else:
                large_B_total = '${:,.2f}'.format(large_pizza_B_total)
                print("")
                print(colored("Your total is: ", "yellow") + large_B_total)

            print("")
            print(colored("When prompted for payment: ", "yellow")) 
            print("Enter (C) for Cash ")
            print("      (S) for Student Meal Pass  ")
            print("")
            print("")
            payment_type = input(colored("Would you like to pay by cash or Student Meal Pass?  \n  ", "yellow"))
            if payment_type == "C" or payment_type == "c":
                print("")
                print(colored("Example: To pay $30 enter 30.00 or 30 ", "yellow"))
                cash = input(colored("Enter the amount you would like to pay in cash:  ", "yellow"))
                cash_E = cash.replace('.', ' ')
                cash_E1 = cash_E.replace(' ', '')
                if int(cash_E1) < int(large_B):
                    print("Invalid entry.")
                    print("")
                    cash2 = input(colored("Enter a valid amount to pay for the order.  \n  ", "yellow"))
                    cash2_E = cash2.replace('.', ' ')
                    cash2_E1 = cash2_E.replace(' ', '')
                    if int(cash2_E1) < int(large_B):
                        print("Invalid entry. Order canceled.")  
                    else:
                        if int(cash2_E1) > int(large_B):
                            cash_back = int(cash2_E1) - int(large_B) 
                            print("")
                            print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
                            print("")
                            print("")
                else:
                    print("")
                    print(colored("Thank you! Your order will be delivered shortly.", "yellow"))
            elif payment_type == "S" or payment_type == "s":
                print("")
                student_id = input("Enter your Student ID: ") 
                print("")
                print("thank you! Your order will arrive shortly.")
                print("The total will be deducted from your student account.")
                print("")
                print("")
                print("")
            else:
                print("Invalid entry.")















