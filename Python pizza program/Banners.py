# Banners.py

from time import sleep
from clear import clear
from termcolor import colored

def main_banner():
    clear()
    print("")
    print("")
    print("")
    print(colored("=================================================================", "yellow"))
    print(colored("==========                                             ==========", "yellow"))
    print(colored("==========      O R D E R    P I Z Z A    W I T H      ==========", "yellow"))
    print(colored("==========                                             ==========", "yellow"))
    print(colored("==========                  P Y T H O N                ==========", "yellow"))
    print(colored("==========                                             ==========", "yellow"))
    print(colored("=================================================================", "yellow"))
    print("")
    print("")
    print("")
    sleep(3)
    clear()

