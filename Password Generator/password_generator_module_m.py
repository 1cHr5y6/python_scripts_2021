# password_generator_module.py

import random
from termcolor import colored

def password_generator_function():
    while True:
        try:
            letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

            numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

            symbols = ['!', '#', '$', '%', '&', '*', '(', ')', '+']

            nr_letters = int(input(colored("How many letters would you like in your password?:\n", "cyan")))
            print("")
            while True:
                try: 
                    nr_numbers = int(input(colored("How many numbers would you like in your password?:\n", "cyan")))
                    break
                except:
                    print("Invalid selection.")                    

            print("")
            while True: 
                try:
                    nr_symbols = int(input(colored("How many symbols would you like in your password?:\n", "cyan")))
                    break
                except: 
                    print("Invalid selection.")
            print("")

            password_list = []

            for char in range(1, nr_letters +1):
                password_list.append(random.choice(letters))

            for char in range(1, nr_symbols + 1):
                password_list += random.choice(symbols)
        
            for char in range(1, nr_numbers + 1):
                password_list += random.choice(numbers)

            random.shuffle(password_list)

            password = ""
            for char in password_list:
                password += char
                sPassword = ' '.join(password)
   
            print("")
            print("") 
            print('\x1b[2;30;44m' + f"Your password is:\n"  + '\x1b[0m')
            print(sPassword)
            print("")
            print("")
            print("")
            break

        except:
            print("Invalid selection.")
