# Banners.py

from clear import clear
from time import sleep
from termcolor import colored

def main_banner():
    sleep(3)
    clear()
    print(colored("==================================================================", "cyan"))
    print(colored("=====                                                        =====", "cyan")) 
    print(colored("=====            P a s s w o r d    G e n e r a t o r        =====", "cyan")) 
    print(colored("=====            ------------------------------------        =====", "cyan"))
    print(colored("=====                     written in Python                  =====", "cyan")) 
    print(colored("=====                                                        =====", "cyan")) 
    print(colored("==================================================================", "cyan"))
    print("")
    print("")

