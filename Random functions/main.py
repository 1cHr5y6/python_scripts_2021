import random
from Banners import main_banner
from termcolor import colored
# original source code for project: www.jquery-az.com/python-random/


main_banner()

random_number = random.randrange(500)
print(colored("Random number generator:       ", "red") + str(random_number))

print("")

random_number_even = random.randrange(10,100,2)
print(colored("Random even number generator:  ", "red") + str(random_number_even))

print("")

random_number_odd = random.randrange(1,500,2)
print(colored("Random odd number generator:   ", "red") + str(random_number_odd))

print("")
print("")
random_float_number = random.random()
print(colored("Random floating point number:                       ", "red") + str(random_float_number))

print("")

# Generate random floating point number between 0.9 and 5.0
random_float_num = random.random() + 5
print(colored("Random floating point number between a range:       ", "red") + str(random_float_num))

print("") 

# Generate random floating point number and 'b' value may be included 
random_float_num2 = random.uniform(10.5,20.5)
print(colored("Random floating integer, value \'b\' may be included: ", "red") + str(random_float_num2))

print("")
print("")

# Generates random number from a list
random_list = [5832,5893,2839,8938,1083,8183,7883,6563,3848,2383]
random_num_list = random.choice(random_list)
print(colored("Random number from list: ", "red") + str(random_num_list))

print("")

random_shuffle = [1,2,3,4,5,6,7,8,9,10]
print(colored("List of numbers before shuffling: ", "red") + str(random_shuffle))
random_shuffle_num = random.shuffle(random_shuffle)
print(colored("List of numbers after shuffle:    ", "red") + str(random_shuffle))

print("")








