# Banners.py

from termcolor import colored

def main_banner():
    print("")
    print("")
    print(colored("==================================================================================", "red")) 
    print(colored("==========                                                              ==========", "red"))
    print(colored("==========                                                              ==========", "red"))
    print(colored("==========                    PYTHON's RANDOM METHOD                    ==========", "red"))
    print(colored("==========                                                              ==========", "red"))
    print(colored("==========                                                              ==========", "red"))
    print(colored("==================================================================================", "red")) 
    print("")
    print("")

